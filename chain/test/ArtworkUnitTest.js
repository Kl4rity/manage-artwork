const Artwork = artifacts.require('Artwork');

// Setting up Chain-Specific Assertions
const truffleAssert = require('truffle-assertions');

// Setting up Chai
const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')
chai.use(chaiAsPromised)
const expect = chai.expect

contract("Artwork - given existing artwork", accounts => {
    let userOwningArtwork = accounts[1]
    let authorizedUser = accounts[2]
    let userTransferredTo = accounts[3]
    let unAuthorizedUser= accounts[4]
    let authorizedForAllUser = accounts[5]

    let instance;
    let artworkId;


    before(async ()=>{
        instance = await Artwork.deployed()
    })

    beforeEach(async ()=>{
        artworkId = await mintArtworkForUser(instance, userOwningArtwork)
    })

    it("when a new artwork is minted, then user owns artwork", async () => {
        let setOwnerOfArtwork = await instance.ownerOf(artworkId)
        assert.equal(userOwningArtwork, setOwnerOfArtwork)
    })

    it("when unauthorized account tries to transfer artwork, then error is thrown", async () => {
        await expect(
            instance.safeTransferFrom(userOwningArtwork, userTransferredTo, artworkId, {from: unAuthorizedUser})
        ).to.be.rejectedWith(Error)
    })

    it("given user with approveAll, when transfer is triggered, transfer succeds", async () => {
        let trxReceipt = await instance.setApprovalForAll(authorizedForAllUser, true, {from: userOwningArtwork})

        truffleAssert.eventEmitted(trxReceipt, 'ApprovalForAll', (ev) =>{
            return ev.operator === authorizedForAllUser
        })
    })

    it("given existing artwork, when user destroys artwork, then interactions with it fail", async () => {
        let trxReceiptBurn = await instance.burn(artworkId, {from: userOwningArtwork})

        truffleAssert.eventEmitted(trxReceiptBurn, 'Transfer', (ev) => {
            return ev.from === userOwningArtwork && ev.to === "0x0000000000000000000000000000000000000000"
        })

        await expect(
            instance.safeTransferFrom(userOwningArtwork, userTransferredTo, {from: userOwningArtwork})
        ).to.be.rejectedWith(Error)

        await expect(
            instance.ownerOf(artworkId)
        ).to.be.rejectedWith(Error)

        await expect(
            instance.approve(authorizedUser, artworkId, {from: userOwningArtwork})
        ).to.be.rejectedWith(Error)
    })

    it("when authorizing another account to move artwork, then Approval event is emitted", async () => {
        let trxReceipt = await instance.approve(authorizedUser, artworkId, {from: userOwningArtwork})

        truffleAssert.eventEmitted(trxReceipt, 'Approval', (ev) =>{
            return ev.approved === authorizedUser
        })

        let approvedUser = await instance.getApproved(artworkId)
        assert.equal(approvedUser, authorizedUser)
    })

    it("given approved user, when approvedUser transfers artwork, then Transfer is executed", async () => {
        let trxReceiptApproval = await instance.approve(authorizedUser, artworkId, {from: userOwningArtwork})

        truffleAssert.eventEmitted(trxReceiptApproval, 'Approval', (ev) =>{
            return ev.approved === authorizedUser
        })

        let trxReceipt = await instance.safeTransferFrom(userOwningArtwork, userTransferredTo, artworkId, {from: authorizedUser})

        truffleAssert.eventEmitted(trxReceipt, 'Transfer', (ev) =>{
            return ev.to === userTransferredTo
        })

        let newOwner = await instance.ownerOf(artworkId)
        assert.equal(userTransferredTo, newOwner)
    })
});

async function mintArtworkForUser(instance, userOwningArtwork) {
    let trxReceipt = await instance.mint(userOwningArtwork)

    let idOfArtwork;
    truffleAssert.eventEmitted(trxReceipt, 'Transfer', (ev) => {
        let transferToOwningAccountExists = ev.to === userOwningArtwork
        if (transferToOwningAccountExists) {
            idOfArtwork = ev.tokenId.words[0]
        }
        return transferToOwningAccountExists
    })

    return idOfArtwork
}
