# Chain
This folder contains chain-related code.

## Outline
The implementation strictly inherits from the preset OpenZeppelin ERC721 implementation. 
No modifications have been made.

## Tooling
### Truffle
Truffle is used for managing the lifecycle of the contract. Download instructions can be found (here)[https://www.trufflesuite.com/truffle].

### OpenZeppelin (managed via npm)
Used for building on battle-tested standard-implementations of contracts.

## Deploying the contract

Assuming you have the necessary tools installed, to deploy the contract locally, you need to...
1. `(cd .. & docker-compose up)`
2. `truffle compile`
3. `truffle migrate`

Voilá, your contract is deployed to the Ganache Test-Chain running locally.

## Debugging the tests

To debug the truffle tests, you need to create a new IntelliJ configuration according to the picture below.

![debug-config](./doc-assets/debug-config.png)
