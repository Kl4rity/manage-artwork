# manage-artwork

The goal of this repository is to house a service which allows artists to put their artwork onto a Blockchain and to, subsequently, allow them and other parties involved to prove the provenance of a specific piece of artwork as well as allow artists to participate in the re-sale of their piece of art.

## How should it work?

There are three actors in this system.

* CREATOR: The artist who would like to put his physical piece of art into the digital trading ecosystem.
* OPERATOR: The operators of the ecosystem.
* OWNER: Anyone owning or wanting to own an artwork: The people operating in the secondary market, buying and selling or collecting artwork as well as the artist creating the artwork.

A piece of art enters the ecosystem by being appraised by the OPERATOR. The OPERATOR guarantees that this is an original and
mints an NFT for this specific piece of art of the Blockchain. The physicial piece of art and the NFT are connected
via an ID which will be attached to the artwork and cannot be removed without breaking it.

When the artwork is sold, the NFT is transferred to it's new owner. Legal ownership is proven by ownership of the NFT.

Ideally, prices would be paid on the blockchain in ETH or any other currency that can be handled by ETH. This would allow
the transfer Method to incurr a fee for the artist who originally created the piece of art, allowing him to benefit off the secondary
market. 
