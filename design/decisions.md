# Artwork Management

## Real world objects on the chain

In order to connect real world artwork with an NFT on the chain, a connecting
identifier needs to be artificially created. A real world artwork has no inherent property
that could always be created the same way and would therefore uniquely identify it - any conversion of a
physical good into a digital representation is an abstraction that cannot be replicated reliably so as to make it an ID.

Hence, UUIDs will be created and attached to the artwork with a seal that cannot be removed from the artwork and is,
therefore, bound to it until the artwork or the seal are destroyed.  
