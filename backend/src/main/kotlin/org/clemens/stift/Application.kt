package org.clemens.stift

import io.micronaut.runtime.Micronaut.*

fun main(args: Array<String>) {
	build()
	    .args(*args)
		.packages("org.clemens.stift")
		.start()
}

